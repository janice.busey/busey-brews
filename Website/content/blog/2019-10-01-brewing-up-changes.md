---
title: "Brewing Up Changes"
date: 2019-10-01
tags: Article
author: Jeffrey V. Smith
image: "blog/2019-10-01-brewing-up-changes.jpg"
---

Thanks to MMAC for the write up on page 14 of the [October issue of their magazine](https://issuu.com/wideawakemedia/docs/mmacoctober2019)!

**NEDERLAND**

In the middle of October, brothers Kyle and Nick Busey transform James Peak Brewery & Smokehouse into Busey Brews. There are plenty of changes and adjustments planned, including making a lot more beer, but the smoked meats and other features will remain. Celebrate the opening of Colorado’s newest brewery, Oct. 19, with food, beer and music.

The Busey brothers, who grew up in Amish Country in Lancaster, PA, moved to Nederland in 2008, and Kyle has been there ever since. Kyle, who has been running Grow in Peace in town “for years,” has more than 22 years of restaurant experience “in every position possible,” a food and beverage management degree and “a love for service.” Nick, a computer programmer and CTO of a tech startup in Boulder, is an avid home-brewer. 

The inspiration for Busey Brews began when Kyle was hired as General Manager at James Peak this past spring. Although the brewery had not produced a beer in over a year, he decided with the help of his home-brewing brother and their friend Judd Kaufman, founder of Gold Dirt Distillery in Rollinsville, they could “re-ignite” the brewing program. They were right. 

“Over the course of the summer, I pursued the purchase of the brewery from the current owners, and on Aug. 30, the sale was finalized,” Kyle explained. “With my extensive restaurant background and Nick’s passion for brewing, operating a brewery in Nederland was a no-brainer for us. This is our home and we want to help make it a little bit better through beer, friends and barbecue.”

Kyle is excited about the changes he and his brother have planned. “We know this location has a bit of a storied past, but we plan on making some serious improvements and changes in the way things operate. We hope you notice a big difference in your experience with us and recognize we are not just changing out the sign and calling it a day,” he said.

The transformation to Busey Brews should be evident right away. “You will certainly see more beer,” Kyle said. “We have a brand-new fermenter and are upgrading various aspects of the brewing and tap system to allow us to brew and serve better beer than was possible in the past. You can also expect to see a barrel aging program in the near future. On the food side of things, the smoked meats certainly aren’t going anywhere. We are planning on adding a second smoker to double our capacity… as we can barely keep up with the demand for our food.”

In addition to beer, the brothers plan to ferment other “delicious” forms of drinks and food as well. Look for house-fermented hot sauces and pickles “very soon,” as well as house-made kombucha and natural soda like birch and root beer.

“We are also going to be installing a hydroponic garden to allow us to grow fresh greens year-round in addition to working with local farms to source as many ingredients locally as possible,” Kyle said.

Due to licensing issues, Busey Brews can’t brew beer at its facility for opening day, Oct. 19, 11 a.m.-9 p.m., but other breweries have agreed to help with collaboration brews. For opening day, beer styles include, “Dragon Well Kölsch” from Jade Mountain, a New England-style IPA from Odd13, an Imperial Red IPA from Phantom Canyon and a kettle sour from Vision Quest. According to Nick, on-going beer styles will be “varied, seasonal and changing often.” The Brittney Wagner Duo performs during the event, 5-7 p.m.

Beginning Oct. 19, Busey Brews, 70 E 1st St., is open Sunday-Thursday, 11 a.m.-8 p.m., and Friday-Saturday, 11 a.m.-9 p.m. Visit buseybrews.com and facebook.com/BuseyBrews or call 855-NED-BREW to learn more.

![Scan](/blog/2019-10-01-brewing-up-changes.scan.jpg)
