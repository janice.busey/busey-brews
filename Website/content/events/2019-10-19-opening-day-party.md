---
title: "Opening Day Party!"
date: 2019-10-19
tags: Events
author: Busey Brews
image: "events/2019-10-19-OpeningDayParty.jpg"
---

Join us as we celebrate our official opening as Busey Brews!

Due to licensing problems, we are unable to brew beer at our facility for the opening day, but thankfully several local breweries have agreed to help us out with collaboration brews!

We will have custom collaborations on tap from Phantom Canyon Brewing Company, VisionQuest Brewery, Jade Mountain Brewing Company, Lumpy Ridge Brewing Company, Odd13 Brewing, and more!

Music by the Brittney Wagner Duo (5-7pm)
Brittney's Blue Ridge Mountains background inspired her to create her own style of Blues. Jon Ridnell (Blackdog) will be backing her up for harmonies and guitar playing that will impress and amuse.

[Facebook Event](https://www.facebook.com/events/1096086663918498/)