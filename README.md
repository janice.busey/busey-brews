# Busey Brews

This repository holds everything we can open source from our brewery.

https://buseybrews.com/

## Website/

This folder contains the buseybrews.com website files.

Run `make web` folder to develop the website locally.

## Documentation/

This folder contains our documentation, like recipes and SOPs.

Run `make docs` in the root of the repository to develop the documentation locally.

## Logo/

Our logo files.