# Open Source Brewery

We are an open source brewery.

## Why?

1. We believe in giving back to the beer community and greater community, and this is one small way to do that.
1. We don't have any secrets. Beer recipes aren't magic. Why make something a secret when it has no reason to be.
1. We can only get better by getting feedback from the community.
1. We believe consumers appreciate the transparency.