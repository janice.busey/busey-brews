# Beer Transfer

**Last Updated: 2019-10-09 by Nick Busey**

## Object and Scope

To ensure a consistent, reliable transferring process.

## Equipment Needed

1. Cart Pump
1. 2 x Medium (12') Beer Hoses
1. Fining agent

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** None

**Hazards:** Pressure! Ensure the receiving tank has a top valve open so gas can escape and not build up.
Ensure the fermenter has an opening to prevent a vacuum from forming.

## Preparation

1. Ensure your receiving tank has been [cleaned](TankCleaning.md) and [sanitized](TankSanitization.md) before beginning.

## Steps

1. Add fining agent to receiving tank if applicable. The incoming beer will mix it up.
1. With the bottom valve on the receiving tank closed and the top valve slightly open, turn on the Co2 to the carbonating stone and allow it to run for two minutes. This purges the tank of any oxygen.
1. Attach the long hose that was used to sanitize the tank to the butterfly valve A of the [transfer tee](TransferTee.md) on the serving tank.
1. 

## Reference Links

None yet