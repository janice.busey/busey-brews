# Tank Cleaning

**Last Updated: 2019-10-09 by Nick Busey**

## Object and Scope

The purpose of this SOP is to ensure consistency of process relating to the CIP, Sanitization and Purging/Pressurization of a 7 BBL Fermenter.

## Equipment Needed

1. Cart Pump
1. 2 x Short Cleaning Hoses

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** Acid, Caustic, Co2

**Hazards:** Handling hazardous products, heat

## Preparation

1. Ensure tank is empty before beginning.
* Open dump port slowly to release anything that may be in the tank.
* Open port fully.
* Remove fermenter door and gasket.
    * Take out of tank room and clean separately with a brush and caustic.
* Hose out any solids remaining in fermenter.
* Connect dump port to [transfer tee](TransferTee.md).
* Connect other side of transfer tee to pump inlet.
* Connect pump outlet to CIP inlet.
* Close transfer tee dump side.

## Cleaning Fermenters

### CIP
1. Fill tank with 15 gallons hot water from the hose.
    * Our hose runs about 5 gallons a minute, so run it full blast for 3 minutes.
* Add super CIP to tank.
* Replace fermenter door and gasket.
* Let loop run for 30 minutes.
* Stop pump and dump the liquid.

### Rinse
1. Fill HLT with city water.
* Pump HLT to fermenter CIP arm.
* Let loop run for 30 minutes.
* Stop pump and dump the liquid.

### Wrap up
1. Open dump port, open front door.
* Remove all hoses and tubes.
* Leave open to dry ready to be sanitized before the next use.

## Reference Links

[Baerlic Brewing Co SOP: Brite Tank CIP](https://docs.google.com/document/d/104BeZZ-xZyXBBXoZETXykyxGHBYdOG-0vbc_wi7WzX8/)