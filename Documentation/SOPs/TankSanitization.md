# Tank Sanitization

**Last Updated: 2019-10-09 by Nick Busey**

## Object and Scope

Sanitization is one of the most crucial processes in any brewery. Less than sanitary conditions
can lead to beer spoilaged and other nastiness. This document defines our process for sanitization.

## Equipment Needed

1. Cart pump
1. 2 x Cleaning Hoses
1. Starsan

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** Sanitizer

**Hazards:** Handling hazardous products, heat, pressure.

**Pressure can build when heating a cold tank! A vacuum can occur when cooling a hot tank!**

You should **always** have a tri-clamp open to the tank to let the air equalize and let off any pressure that builds
up with looping a hot sanitizer.

## Preparation

1. Ensure the [tank has been cleaned](TankCleaning.md).
1. Rinse out the tank
1. Add tri-clamp to the bottom port on the tank, ensure it is closed.
1. Ensure any other parts like the tasting port, carb stone, etc. have been cleaned, sanitized, and installed properly before beginning.
1. Attach the cleaned racking arm and turn it so it is pointing up.
    * Open the butterfly valve on the racking arm all the way.
1. Open the sample port, leave it open so sanitizer runs through it.

## Steps

1. Add 15 gallons hot water to the tank.
1. Add 3 ounces StarSan to the water.
1. Attach a cleaning hose to the bottom port of the tank and the pump inlet.
1. Attach a cleaning hose to the pump outlet and to the CIP arm inlet.
1. Ensure all butterly valves in the CIP loop are open, and start the pump.
1. Once the loop is running, close the butterly valve on the racking arm to allow it to fill with sanitizer.
1. Run the loop for 30 minutes.
1. Remove pump hoses.
1. Drain sanitizer from bottom port.
1. Rotate racking arm down to allow any sanitizer to drain.
1. Shut the sample port.
1. Remove 

## Reference Links

* [Writing it Down! SOPs: The Foundation of Any Brewery Quality Program](https://www.brewersassociation.org/seminars/writing-it-down-sops-the-foundation-of-any-brewery-quality-program/)
* [SOP Template](https://docs.google.com/document/d/1CGs0_PrNgRo6sNEkFQLKV4aML2P1DC64LLdagqQAwrk/edit#heading=h.ni8nbliewtnr)
