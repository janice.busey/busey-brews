# How to make Kombucha

## Ingredients

* 2 Gallon
* 1 Large Iced Tea Bag
* 450 Grams Sugar
* 4 Cups Starter Fluid
* Scoby that covers at lesat 25% of the surface of the container
* Clean 3 Gallon Plastic Container
* Clean White Rag to use as Air Lock
* Black O-Ring or Rubber Band to attach White Rag

## Steps 

* Bring 1/2 of the total volume to a near boil
* Stop heat, add sugar, stir to dissolve.
* Add tea bags, steep at least 20 minutes
* Remove bag carefully, so it doesn't tear.
    * If you do tear the bag, strain it through some cheese cloth.
* Cover with lid and wait for it to cool to luke warm temperature.
* Add scoby.
* Pour starter fluid on top of the scoby, slowly and gently, to form a 'seal' of the starter fluid on top of the container.
* Let ferment one week.
* Taste daily until desired acidity is reached.

### Note

Some day soon we want to change this to a pH based process.