# Working with our Documentation

All our documentation is open source, which means anyone
can submit suggestions for changes and additions.

All you have to do is click the pencil icon on the top right of the page. It will take you to GitLab, where you can make changes, then submit them for consideration.

If you don't have a GitLab account, you can easily login
with Google or GitHub.

## Mermaid

You can add a Mermaid chart like so:

```
<div class="mermaid">
graph TD
    subgraph Prep
        A[Ensure tank is empty]
        A --> B[Empty dump port]
    end
    subgraph CIP
        B --> D{Rinse}
        D --> G[Testing]
    end
    D --> E[Wrap Up]
</div>
```

this renders a chart like:

<div class="mermaid">
graph TD
    subgraph Prep
        A[Ensure tank is empty]
        A --> B[Empty dump port]
    end
    subgraph CIP
        B --> D{Rinse}
        D --> G[Testing]
    end
    D --> E[Wrap Up]
</div>
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>