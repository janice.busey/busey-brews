.PHONY: web docs build_web build_docs

web:
	cd Website && hugo serve

docs:
	mkdocs serve

build_web:
	cd Website && hugo build

build_docs:
	mkdocs build